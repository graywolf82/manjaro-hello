<big>Manjaro 20.0</big>

We are happy to publish another stable release of Manjaro Linux, named Lysia.

The Xfce edition remains our flagship offering and has received the attention it deserves. Only a few can claim to offer such a polished, integrated and leading-edge Xfce experience. With this release we ship Xfce 4.14 and have mostly focused on polishing the user experience with the desktop and window manager. Also we have switched to a new theme called Matcha. A new feature Display-Profiles allows you to store one or more profiles for your preferred display configuration. We also have implemented auto-application of profiles when new displays are connected.

Our KDE edition provides the powerful, mature and feature-rich Plasma 5.18 desktop environment with a unique look-and-feel, which we completely re-designed in 2020. The full set of Breath2-themes includes light and dark versions, animated splash-screen, Konsole profiles, Yakuake skins and many more little details. We have rounded off text editor Kate with some additional color schemes and offer Plasma-Simplemenu as an alternative to the traditional Kickoff-Launcher. With a wide selection of latest KDE-Apps 20.04 and other applications Manjaro-KDE aims to be a versatile and elegant environment ready for all your everyday needs.

With our Gnome edition based on 3.36 series we include visual refreshes for a number of applications and interfaces, particularly noteworthy being the login and unlock interfaces. GNOME shell extensions are now managed using a new Extensions app which handles updating, configuring and removing or disabling extensions. A Do Not Disturb button was added to the notifications popover. When enabled, notifications are hidden until the button is toggled off. By default our own dynamic wallpaper changes its colour theme throughout the day. Additionally we updated GDM and improved our Gnome-Layout-Switcher a lot. We updated our list of pre-installed packages, zsh is the new default shell and applications are now sorted in folders in a clean app drawer.

Pamac 9.4 series received a few updates. Enhancing our package management we have enabled snap and flatpak support by default. You can now install snaps or flatpaks very easily with Pamac in UI and terminal to make more use of an even much larger selection of latest Linux applications. For Gnome users we enabled similar features as supported by Gnome-Software to get more package information by simply right clicking them in gnome-shell app drawer. And if you search for an app you don't have, pamac offers to install it for you.

Manjaro Architect supports now ZFS installation by providing the needed kernel modules.

Kernel 5.6 is used for this release, such as the latest drivers available to date. Relative to the last installation media release, our tools have been improved and polished.

We hope you enjoy this release and let us know what you think of Lysia.
